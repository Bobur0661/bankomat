package uz.pdp;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int choose1 = 0;
        BankCard Shox = new BankCard("Bobur", "7777", "1111", 100);
        BankCard Ali = new BankCard("Suhrob", "6666", "9999", 0);

        // Chorsu:
       // BankService chorsu = new BankService(2);
        BankService mega = new BankService(5);

        while (choose1 != 6) {
            System.out.println("************************ BANKOMATGA XUSH KELIBSIZ ************************");
            System.out.println("\n1. Pull qo'shish: ");
            System.out.println("2. Pull yechish: ");
            System.out.println("3. Pull o'tkazish: ");
            System.out.println("4. Balance ni ko'rish: ");
            System.out.println("5. Bankomatning ishlagan puli: ");
            System.out.println("6. Exit: ");
            System.out.print("Yuqoridagi bo'limlardan birini tanlang: ");
            choose1 = scanner.nextInt();
            switch (choose1) {
                case 1: {
                    System.out.print("Qo'shmoqchi bo'lgan summani kiriting: ");
                    scanner = new Scanner(System.in);
                    double money = scanner.nextDouble();
                    System.out.println(mega.fillBalance(Shox, money));
                } break;
                case 2: {
                    System.out.print("Yechmoqchi bo'lgan summani kiriting: ");
                    scanner = new Scanner(System.in);
                    double moneyExtract = scanner.nextDouble();
                    System.out.println(mega.withdraw(Shox, moneyExtract));
                } break;
                case 3: {
                    System.out.print("O'tkizmoqchi bo'lgan karta raqmamini kiriting: ");
                    scanner = new Scanner(System.in);
                    String cardNumber = scanner.nextLine();
                    if (Ali.getCardNumber().equals(cardNumber)) {
                        System.out.print("Nech pul o'tkazmoqchisiz: ");
                        double moneyTrans = scanner.nextDouble();
                        System.out.println(mega.moneyTransfer(Shox, Ali, moneyTrans));
                    }
                } break;
                case 4: {
                    System.out.println("Balansingizda: " + Shox.getBalance());
                } break;
                case 5: {
                    System.out.println("Bankomat ishlagan puli: " + mega.getBalanceOfATM() + " $");
                } break;
            }
        }

        System.out.println(Shox);
    }
}
