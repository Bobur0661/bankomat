package uz.pdp;

public class BankCard {
    private String owner;
    private String password;
    private String cardNumber;
    private double balance;


    // Constructors:
    public BankCard() {
    }

    public BankCard(String owner, String password, String cardNumber, double balance) {
        this.owner = owner;
        this.password = password;
        this.cardNumber = cardNumber;
        this.balance = balance;
    }


    // Getter and Setters:
    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "BankCard{" +
                "owner='" + owner + '\'' +
                ", password='" + password + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                ", balance=" + balance +
                '}';
    }
}
