package uz.pdp;

public class BankService {

    private double percent;
    private double balanceOfATM;

    public BankService(double percent) {
        this.percent = percent;
    }

    public String fillBalance(BankCard anyCard, double sum) {
        if (sum > 0) {
            balanceOfATM += sum * percent / 100;
            anyCard.setBalance(anyCard.getBalance() + (sum - (sum * percent / 100)));
            return anyCard.getCardNumber() + " raqamlik karta " + (sum - (sum * percent / 100)) + " so'mga to'ldirildi";
        }
        return "Manfiy pul miqdorini kirita olmaysiz, Uzr!";
    }


    public String withdraw(BankCard anyCard, double sum) {
        if (anyCard.getBalance() > 0 && anyCard.getBalance() >= (sum + (sum * percent / 100))) {
            if (sum > 0) {
                balanceOfATM += (sum * percent / 100);
                anyCard.setBalance(anyCard.getBalance() - (sum + (sum * percent / 100)));
                return anyCard.getCardNumber() + " raqamli kartadan " + (sum + (sum * percent / 100)) + "$ yechib olindi.";
            } else if (sum == 0) {
                return "Yo'q summani (0) yechib bera olmaymiz. Iltimos 0 dan katta son kiriting.";
            } else {
                return "Manfiy summani yechib ololmaymiz! Please try again.";
            }
        }
        return (anyCard.getCardNumber() + " ==> raqamli kartada yechib olish uchun yetarli pul yo'q.");
    }


    public String moneyTransfer(BankCard fromCard, BankCard toCard, double sum) {
        if (fromCard.getBalance() > 0 && fromCard.getBalance() >= (sum + (sum * percent / 100))) {
            if (sum > 0) {
                balanceOfATM += (sum * percent / 100);
                toCard.setBalance(toCard.getBalance() + sum);
                fromCard.setBalance(fromCard.getBalance() - (sum + (sum * percent / 100)));
                return (fromCard.getCardNumber() + " raqamli kartadan " + (sum) + "$  " + toCard.getCardNumber() +
                        " raqamli kartaga o'tkazildi.");
            } else if (sum == 0) {
                return "Yo'q summani (0) o'tkazib bera olmaymiz. Iltimos 0 dan katta son kiriting.";
            } else {
                return "Manfiy o'tkazib bera olmaymiz! Please try again.";
            }
        }
        return (fromCard.getCardNumber() + " ==> raqamli kartada o'tkazish uchun yetarli pul yo'q.");
    }

    public double getBalanceOfATM() {
        return balanceOfATM;
    }

}
